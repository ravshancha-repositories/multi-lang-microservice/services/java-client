package uz.netflix.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ClientController {
    @Autowired
    private PythonClient pythonClient;

    @GetMapping("api/v1/client/hello")
    public String hello() {
        return pythonClient.hello();
    }
}
