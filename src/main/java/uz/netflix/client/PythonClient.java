package uz.netflix.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(url = "http://localhost:5000", name = "python-client")
public interface PythonClient {
    @GetMapping("/hello")
    String hello();
}
